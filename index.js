
// http://blog.alex-turok.com/2013/08/python-lorenz.html
function lorenz({ rho = 28.0, sigma = 10.0, beta = 8.0 / 3.0, timeDelta = 0.01 } = { }) {
    return (parameters) => {
        const [cRate, hDelta, vDelta] = parameters;
        parameters[0] = (hDelta * timeDelta * sigma) + (cRate * (1 - timeDelta * sigma));
        parameters[1] = (cRate * timeDelta * (rho - vDelta)) + (hDelta * (1 - timeDelta));
        parameters[2] = (cRate * hDelta * timeDelta) + (vDelta * (1 - (timeDelta * beta)));
        return parameters;
    };
}

function *temperature({ min = 28, max = 32, cycles = 1, samples = 360.0 } = { }) {
    const delta = (max - min) / 2.0;
    const turns = 2 * Math.PI * cycles;
    const fraction = turns / samples;
    for (let time = 0; time < samples; time++)
        yield min + delta + (Math.cos(time * fraction) * -delta);
}

module.exports = { lorenz, temperature };